//
//  ViewController.swift
//  ev3_mqtt
//
//  Created by Kevin Chen on 6/11/2019.
//  Copyright © 2019 New York University. All rights reserved.
//

import UIKit
import CocoaMQTT
import Foundation
import Speech

// Threads: OptriTrack, Action

class ViewController: UIViewController {
    // Original Variables
    var client: CocoaMQTT!
    @IBOutlet weak var connectionLabel: UILabel!
    @IBOutlet weak var lengthSlider: UISlider!
    @IBOutlet weak var gridImage: UIImageView!
    @IBOutlet weak var gridView: UIView!
    
    // New Variables
    var position = (0.0,0.0,0.0)
    var orientation = 0.0
    var ideal_orientation = 0.0
    var stop_moving = false
    
    // Following Toy Data
    var toy_position = (0.0,0.0,0.0)
    var toy_orientation = 0.0
    
    var queue_list : [Int] = [ ]
    var queue_parameters : [Any] = [ ] // Need to import Foundation
    
    let fast_movement = "MoveTank 70 70"
    let fast_reverse_movement = "MoveTank -70 -70"
    let slow_movement = "MoveTank 20 20"
    let slow_reverse_movement = "MoveTank -20 -20"
    
    let stop_movement = "MoveTank 0 0"
    
    let slow_right_turn = "MoveTank 10 -10"
    let slow_left_turn = "MoveTank -10 10"
    let fast_right_turn = "MoveTank 20 -20"
    let fast_left_turn = "MoveTank -20 20"
    
    // Init Function
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpMQTT() // MQTT Thread
        action_thread() // Action Thread
    }
    
    // To Do: Switch Case for Functions
    
    func action_thread() {
        DispatchQueue.global(qos: .background).async {
//            // Reset Robot
//            sleep(UInt32(1))
//            self.client.publish("tag/networktest", withString: self.stop_movement)
//
//            // Robot to Origin
//            sleep(UInt32(2))
//            self.moveTo(goal: (0,0), error: 0.2)
//            self.turn(goal: 90)
            
            sleep(UInt32(1))
            // self.moveTo(goal: (4,-3), error: 0.5)
            // self.activity1()
            // self.follow()
            self.main_loop()
            
//            sleep(UInt32(1))
//            while true {
//                self.moveTo(goal: (-5, -3), error: 0.2)
//                self.moveTo(goal: (4,4), error: 0.2)
//            }
        }
    }
    
    // Makes keyboard disappear tapping on screen
    @IBAction func onTap(_ sender: Any) {
        view.endEditing(true)
    }
    
    @IBAction func gridTap(_ sender: UITapGestureRecognizer) {
        var touchPoint = sender.location(in: gridView)
        touchPoint.x = touchPoint.x / self.gridImage.frame.width - 0.5
        touchPoint.y = (touchPoint.y / self.gridImage.frame.height - 0.5) * -1
        
        print("Touched point (\(round(touchPoint.x * 10)), \(round(touchPoint.y * 10)))")
        
        self.queue_list.append(3)
        self.queue_parameters.append((Double(round(touchPoint.x * 10)), Double(round(touchPoint.y * 10))))
    }
    
    func setUpMQTT() {
        let clientID = "CocoaMQTT-" + String(ProcessInfo().processIdentifier)
        client = CocoaMQTT(clientID: clientID, host: "192.168.0.160", port: 1883)
        print("MQTT Set Up", clientID)
        client.username = "nyu"
        client.password = "nyu"
        client.willMessage = CocoaMQTTWill(topic: "/will", message: "dieout")
        client.keepAlive = 60
        client.delegate = self
        client.connect()
    }
    
    @IBAction func get_position(_ sender: Any) {
        let temp = (self.position.0, self.position.1, self.position.2)
        client.publish("tag/networktest", withString: self.stop_movement)
        stop_moving = true
        
        print("I am at ", temp)
        
        var speech = "I am at "
        
        if round(temp.2) < 0 {
            speech.append("negative \(Int(round(abs(temp.2)))) ")
        }
        else{
            speech.append("\(Int(round(abs(self.position.2)))) ")
        }
        if round(self.position.0) < 0 {
            speech.append("negative \(Int(round(abs(self.position.0))))")
        }
        else{
            speech.append("\(Int(round(abs(self.position.0))))")
        }
        
        say(item: speech)
    }
    
    @IBAction func goUp(_ sender: Any) {
        let length = Double(self.lengthSlider.value)
        print("length:",length)
        self.queue_list.append(1)
        self.queue_parameters.append(length)
    }
    
    @IBAction func goDown(_ sender: Any) {
        let length =  Double(self.lengthSlider.value)
        print("length:",length)
        
        self.queue_list.append(1)
        self.queue_parameters.append(-length)
    }
    
    @IBAction func goLeft(_ sender: Any) {
        if (self.ideal_orientation + 90) > 360 {
            self.queue_list.append(2)
            self.queue_parameters.append(90.0)
        }
        else {
            self.queue_list.append(2)
            self.queue_parameters.append(self.ideal_orientation + 90.0)
        }
    }
    
    @IBAction func goRight(_ sender: Any) {
        if (self.ideal_orientation - 90) < 0 {
            self.queue_list.append(2)
            self.queue_parameters.append(270.0)
        }
        else {
            self.queue_list.append(2)
            self.queue_parameters.append(self.ideal_orientation - 90.0)
        }
    }
    
    @IBAction func draw_square(_ sender: Any) {
        let length = Double(self.lengthSlider.value)
        self.queue_list.append(4)
        self.queue_parameters.append(length)
    }
    
    @IBAction func draw_triangle(_ sender: Any) {
        let length = Double(self.lengthSlider.value)
        self.queue_list.append(5)
        self.queue_parameters.append(length)
    }
    
    @IBAction func draw_circle(_ sender: Any) {
        self.queue_list.append(3)
        self.queue_parameters.append((self.toy_position.2,self.toy_position.0))
    }
    
    func say(item: Any) { // Speech
        SpeechService.shared.speak(text: "\(item as! String)", voiceType: .waveNetFemale) { }
    }
    
    func moveTo(goal: (Double, Double), error: Double) {
        var va = (goal.0 - self.position.2, goal.1 - self.position.0)
        var mag = sqrt(pow(va.0, 2) + pow(va.1, 2))
        if mag < error {
            client.publish("tag/networktest", withString: self.stop_movement)
            return
        }
        
        client.publish("tag/networktest", withString: self.fast_movement)
        
        while true {
            if stop_moving {
                stop_moving = !stop_moving
                break
            }
            va = (goal.0 - self.position.2, goal.1 - self.position.0)
            mag = sqrt(pow(va.0, 2) + pow(va.1, 2))
            if mag < error {
                client.publish("tag/networktest", withString: self.stop_movement)
                break
            }
            let angle = atan2(va.1, va.0)
            let diffa = angle * 180 / Double.pi - self.orientation
            let diffr = angle - (self.orientation / 180.0 * Double.pi)
            
            let c = cos(diffr)
            let s = sin(diffr)
            if c < 0 || abs(mag * s) > error {
                self.turn(goal: diffa + self.orientation)
                client.publish("tag/networktest", withString: self.fast_movement)
                let secondsToDelay = mag / 10
                
                DispatchQueue.main.asyncAfter(deadline: .now() + secondsToDelay, execute: {
                })
            }
        }
    }
    
    func move(goal: Double) {
        var length = Double(self.lengthSlider.value)
        
        if goal > 0 {
            length = Double(self.lengthSlider.value)
        }
        else {
            length = Double(-self.lengthSlider.value)
        }
        
//        print("Moving", length, "feet")
        let change_x = length * cos(self.orientation / 180 * Double.pi)
        
        let change_y = length * sin(self.orientation / 180 * Double.pi)
        
        let goa = (self.position.2 + change_x, self.position.0 + change_y)
        
        moveTo(goal: goa, error: 0.2)
        
//        let left_bound_x = goa.0 - 0.07
//        let right_bound_x = goa.0 + 0.07
//        let left_bound_y = goa.1 - 0.07
//        let right_bound_y = goa.1 + 0.07
//
//        if length > 0 {
//            client.publish("tag/networktest", withString: self.slow_movement)
//        }
//        else{
//            client.publish("tag/networktest", withString: self.slow_reverse_movement)
//        }
//
//        while true {
//            if stop_moving {
//                stop_moving = !stop_moving
//                break
//            }
//            if (left_bound_x <= self.position.2 && self.position.2 <= right_bound_x) && (left_bound_y <= self.position.0 && self.position.0 <= right_bound_y) {
//                client.publish("tag/networktest", withString: self.stop_movement)
//                break
//            }
//        }
    }
    
    func turn(goal: Double) {
        let goa = goal - (floor(goal / 360) * 360)
        self.ideal_orientation = goa
        
        // Find out whether to turn left or right based on closest
        let diff = goa - self.orientation
        
        var right = true // False if 0 degrees is right
        if (0 <= diff && diff <= 180) || (-360 <= diff && diff <= -180) {
            right = false
        }
        
        var left_bound = goa - 3
        var right_bound = goa + 3
        var wrap = false
        if (goa - 2 < 0) {
            wrap = true
            left_bound = 360 + left_bound
        }
        else if (goa + 2 > 360) {
            wrap = true
            right_bound = right_bound - 360
        }
        
        if (right) {
            client.publish("tag/networktest", withString: self.slow_right_turn)
        }
        else {
            client.publish("tag/networktest", withString: self.slow_left_turn)
        }
        
        // print(goa)
        while true {
            if stop_moving {
                print("Stop Moving Block")
                stop_moving = !stop_moving
                break
            }
            if wrap {
                if (left_bound <= self.orientation && self.orientation <= 360) || (0 <= self.orientation && self.orientation <= right_bound) {
                    client.publish("tag/networktest", withString: self.stop_movement)
                    print("Wrap Block")
                    break
                }
            }
            else if (left_bound <= self.orientation && self.orientation <= right_bound) {
                client.publish("tag/networktest", withString: self.stop_movement)
                print(left_bound, self.orientation, right_bound)
                print("Regular Escape Block")
                break
            }
        }
    }
    
    func move_to_x(goal: Double) {
        print("Moving in X Direction")
        let left_bound = goal - 0.07
        let right_bound = goal + 0.07
        if (goal - self.position.2 > 0) {
            self.turn(goal: 0)
        }
        else if (goal - self.position.2 < 0) {
            self.turn(goal: 180)
        }
        client.publish("tag/networktest", withString: self.fast_movement)
        while (true) {
            if stop_moving {
                stop_moving = !stop_moving
                break
            }
            if left_bound <= self.position.2 && self.position.2 <= right_bound {
                client.publish("tag/networktest", withString: self.stop_movement)
                break
            }
        }
    }
    
    func move_to_y(goal: Double) {
        print("Moving in Y Direction")
        let left_bound = Double(goal) - 0.07
        let right_bound = Double(goal) + 0.07
        if (Double(goal) - self.position.0 > 0) {
            self.turn(goal: 90)
        }
        else if (Double(goal) - self.position.0 < 0) {
            self.turn(goal: 270)
        }
        client.publish("tag/networktest", withString: self.fast_movement)
        while (true) {
            if stop_moving {
                stop_moving = !stop_moving
                break
            }
            if left_bound <= self.position.0 && self.position.0 <= right_bound {
                client.publish("tag/networktest", withString: self.stop_movement)
                break
            }
        }
    }
    
    func move_to(goal: (Double, Double)) {
        print("Moving to \(goal)")
        move_to_x(goal: goal.0)
        move_to_y(goal: goal.1)
    }
    
    func draw_equi_triangle(length: Double) {
        // let length = Int(self.lengthSlider.value)
        print("Drawing Triangle")
        let area = "\((1/2) * length * length)"
        // 60 Degree Turn
        turn(goal: 60)
        move(goal: 1)
        turn(goal: 300)
        move(goal: 1)
        turn(goal: 180)
        move(goal: 1)
        // Say Area
        say(item: area)
    }
    
    func draw_a_square(length: Double) {
        // let length = Double(self.lengthSlider.value)
        print("Drawing Square")
        let area = "\(length * length)"
//        // Increase Change in Y
//        move_to_y(goal: Double(self.position.0) + length)
//        // Increase Change in X
//        move_to_x(goal: Double(self.position.2) + length)
//        // Decrease Change in Y
//        move_to_y(goal: Double(self.position.0) - length)
//        // Decrease Change in X
//        move_to_x(goal: Double(self.position.2) - length)
        
        turn(goal: 90)
        move(goal: 1)
        turn(goal: 0)
        move(goal: 1)
        turn(goal: 270)
        move(goal: 1)
        turn(goal: 180)
        move(goal: 1)
        
        // Say Area
        say(item: area)
    }
    
    // Previous Queue system-ignore
    func original_loop () {
//            // iOS Infinite Loop
//            while (true) {
//
//                if (!self.queue_list.isEmpty) {
//                    let data = self.queue_list[0]
//                    let split = data.firstIndex(of: " ")!
//                    let act = data[...split]
//                    let action = act.trimmingCharacters(in: .whitespacesAndNewlines)
//                    let par = data[split...]
//                    let param = par.trimmingCharacters(in: .whitespacesAndNewlines)
//                    print(action + " " + param)
//                    if (action == "Move") {
//                        if (param == "Up") {
//                            self.move(goal:1)
//                        }
//                        else if (param == "Down") {
//                            self.move(goal:-1)
//                        }
//                        else {
//                            //self.straight_to(goal: (self.go_to_x, self.go_to_y))
//                            //self.move_to(goal: (self.go_to_x, self.go_to_y))
//                            self.moveTo(goal: (self.go_to_x, self.go_to_y), error: 0.2)
//                        }
//                    }
//                    else if (action == "Rotate") {
//                        if (param == "Left") {
//                            if (self.ideal_orientation + 90) > 360 {
//                                self.turn(goal: 90)
//                            }
//                            else {
//                                self.turn(goal: self.ideal_orientation + 90)
//                            }
//                        }
//                        else {
//                            if (self.ideal_orientation - 90) < 0 {
//                                self.turn(goal: 270)
//                            }
//                            else {
//                                self.turn(goal: self.ideal_orientation - 90)
//                            }
//                        }
//                    }
//                    else if (action == "Draw") {
//                        if (param == "Square") {
//                            self.draw_a_square()
//                        }
//                        else if (param == "Triangle") {
//                            self.draw_equi_triangle()
//                        }
//                        else if (param == "Circle") {
//                            self.follow()
//                        }
//                    }
//                    self.queue_list.removeFirst()
//                }
//            }
    }
    
    func follow() {
        // Robot follows Toy forever
        let error = 0.2
        var goal = (toy_position.2, toy_position.0)
        var va = (goal.0 - self.position.2, goal.1 - self.position.0)
        var mag = sqrt(pow(va.0, 2) + pow(va.1, 2))
        if mag < error {
            client.publish("tag/networktest", withString: self.stop_movement)
            return
        }
        
        client.publish("tag/networktest", withString: self.fast_movement)
        
        while true {
            if stop_moving {
                stop_moving = !stop_moving
                break
            }
            goal = (toy_position.2, toy_position.0)
            va = (goal.0 - self.position.2, goal.1 - self.position.0)
            mag = sqrt(pow(va.0, 2) + pow(va.1, 2))
            if mag < error {
                client.publish("tag/networktest", withString: self.stop_movement)
                // break
            }
            let angle = atan2(va.1, va.0)
            let diffa = angle * 180 / Double.pi - self.orientation
            let diffr = angle - (self.orientation / 180.0 * Double.pi)
            
            let c = cos(diffr)
            let s = sin(diffr)
            if c < 0 || abs(mag * s) > error {
                self.turn(goal: diffa + self.orientation)
                client.publish("tag/networktest", withString: self.fast_movement)
                sleep(UInt32(0.5))
            }
        }
    }
    
    func activity1 () {
        // Student Activity: Go to (x,y) and correct them
        let x = 4
        let y = 3
        self.say(item: "Where is \(x) comma \(y)?")
        sleep(UInt32(6))
        let toy_pos = (Double(((self.toy_position.2))), Double(((self.toy_position.0))))
        self.moveTo(goal: (Double(toy_pos.0), Double(toy_pos.1)), error: 0.2)
        let robot_pos = (Int(round((self.position.2))), Int(round((self.position.0))))
        if robot_pos == (x,y) {
            self.say(item: "Good Job! That is correct. You are at \(x) comma \(y).")
        }
        else {
            self.say(item: "Good try. But that is not correct. You are at \(robot_pos.0) comma \(robot_pos.1).")
            sleep(UInt32(5))
            self.moveTo(goal: (Double(x),Double(y)), error: 0.2)
            self.say(item: "This is \(x) comma \(y).")
        }
    }
    
    func main_loop () {
        // Switch case
        while (true) {
            if (!self.queue_list.isEmpty) {
               
                
                let action = self.queue_list[0]
                print(self.queue_list)
                print(self.queue_parameters)
                let param = self.queue_parameters[0]
              
            
                

                switch (action) {
                case 1:
                    self.move(goal: param as! Double)
                case 2:
                    self.turn(goal: param as! Double)
                case 3:
                    self.moveTo(goal: param as! (Double, Double), error: 0.2)
                case 4:
                    self.draw_a_square(length: param as! Double)
                case 5:
                    self.draw_equi_triangle(length: param as! Double)
                default:
                    self.say(item: "Error")
                }

                self.queue_list.removeFirst()
                self.queue_parameters.removeFirst()
            }
        }
    }
}

// MQTT Integration
extension ViewController: CocoaMQTTDelegate {
    // These two methods are all we care about for now
    func mqtt(_ mqtt: CocoaMQTT, didReceiveMessage message: CocoaMQTTMessage, id: UInt16 ) {
        // print(self.queue_list)
        
        let msg = message.payload
        var array : [Float64] = [0.0, 0.0, 0.0, 0.0]
        memcpy(&array, msg, 32)
        var id = UInt32(0)
        id += UInt32(msg[32]) + (UInt32(msg[33])<<8) + (UInt32(msg[34])<<16) + (UInt32(msg[35])<<24)
        
        if (id == 1) {
            self.orientation = array[0]
            self.position = (array[1], array[2], array[3])
        }
        else {
            self.toy_orientation = array[0]
            self.toy_position = (array[1], array[2], array[3])
        }
    }
    
    // Other required methods for CocoaMQTTDelegate
    func mqtt(_ mqtt: CocoaMQTT, didReceive trust: SecTrust, completionHandler: @escaping (Bool) -> Void) {
        completionHandler(true)
    }
    
    func mqtt(_ mqtt: CocoaMQTT, didConnectAck ack: CocoaMQTTConnAck) {
        connectionLabel.text = "Connected"
        connectionLabel.textColor = UIColor.green
        self.client.subscribe("tag/optitrack_data")
    }
    
    func mqtt(_ mqtt: CocoaMQTT, didPublishMessage message: CocoaMQTTMessage, id: UInt16) {
    }
    
    func mqtt(_ mqtt: CocoaMQTT, didPublishAck id: UInt16) {
    }
    
    func mqtt(_ mqtt: CocoaMQTT, didSubscribeTopic topic: String) {
    }
    
    func mqtt(_ mqtt: CocoaMQTT, didUnsubscribeTopic topic: String) {
    }
    
    func mqttDidPing(_ mqtt: CocoaMQTT) {
    }
    
    func mqttDidReceivePong(_ mqtt: CocoaMQTT) {
    }
    
    func mqttDidDisconnect(_ mqtt: CocoaMQTT, withError err: Error?) {
        self.client.disconnect()
    }
    
    func _console(_ info: String) {
    }
}
