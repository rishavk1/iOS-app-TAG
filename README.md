# iOS App for TAG
- [Setup MQTT Server](#Setup-MQTT)
- [Functions](#Functions)

## Setup MQTT
Website Source for MQTT in iOS: https://medium.com/thefloatingpoint/mqtt-in-ios-d8574b55e006
1) Cocoa Pod: Install CocoaMQTT (pod init, pod CocoaMQTT, pod install)
2) Setup Function (add in viewcontroller.swift)
```
func setUpMQTT() {
    let clientID = "CocoaMQTT-" + String(ProcessInfo().processIdentifier)
    mqtt = CocoaMQTT(clientID: clientID, host: "localhost", port: 1883)
    mqtt.username = "nyu"
    mqtt.password = "nyu"
    mqtt.willMessage = CocoaMQTTWill(topic: "/will", message: "dieout")
    mqtt.keepAlive = 60
    mqtt.connect()
}
```
3) Add extension/delgate functions
```
func mqtt(_ mqtt: CocoaMQTT, didReceiveMessage message: CocoaMQTTMessage, id: UInt16 ) {
    let msg = message.payload
    var array : [Float64] = [0.0, 0.0, 0.0, 0.0]
    memcpy(&array, msg, 32)
    var id = UInt32(0)
    id += UInt32(msg[32]) + (UInt32(msg[33])<<8) + (UInt32(msg[34])<<16) + (UInt32(msg[35])<<24)

    if (id == 1) {
        self.orientation = array[0]
        self.position = (array[1], array[2], array[3])
    }
    else {
        self.toy_orientation = array[0]
        self.toy_position = (array[1], array[2], array[3])
    }
}
```
* didReceiveMessage receives the steamed data and updates the global position and orientation variables

## Functions
```
viewDidLoad()
```
* Sets up the MQTT thread and the EV3 Robot thread upon the app opening
```
setUpMQTT()
```
* Sets up the EV3 Robot thread
```
action_thread()
```
* 2nd Thread that sends commands to the robot to perform actions
```
main_loop()
```
* Performs each action on the queue, then dequeues it.
```
gridTap()
```
* UI
* Robot moves to the tapped grid location
```
get_position()
```
* Button on iOS app that triggers to speak out the position of the robot in coordinate points using OptiTrack position data
```
goUp()
```
* Button on iOS app that triggers function to move forward X amount of feet using OptiTrack position data
```
goDown()
```
* Button on iOS app that triggers function to move down X amount of feet using OptiTrack position data
```
goLeft()
```
* Button on iOS app that triggers function to turn left 90 using OptiTrack orientation data
```
goRight()
```
* Button on iOS app that triggers function to turn right 90 using OptiTrack orientation data
```
draw_square()
```
* Button on iOS app that triggers function to draw a square X length using OptiTrack position and orientation data
```
draw_triangle()
```
* Button on iOS app that triggers function to draw a square X length using OptiTrack position and orientation data
```
draw_circle()
```
* Currently incomplete
```
moveTo(goal: Tuple)
```
* Function that moves straight to desired coordinate (goal)
* Robot adjusts itself as it moves towards the desired coordinate point. Currently stops, turns, moves, instead of arcing
```
move(goal: Double)
```
* Function that moves straight X length (goal)
* Robot keeps moving until the position is within two bounds that marks the destination coordinate
```
turn(goal: Double)
```
* Function that turns to X orientation (goal)
* Robot keeps turning until the position is within two bounds that marks the desired Orientation
```
move_to_x(goal: Double)
```
* Function that moves to x position (goal)
* Robot keeps moving until the position is within two bounds that marks the destination x point
```
move_to_y(goal: Double)
```
* Function that moves to y position (goal)
* Robot keeps moving until the position is within two bounds that marks the destination y point
```
move_to(goal: Tuple)
```
* Function that doesn't move straight to coordinate point, but uses the x-axis and y-axis to get to coordinate (goal)
* Robot calls move_to_x and move_to_y
```
draw_equi_triangle(length: Double)
```
* Function that draws a equilateral triangle with X length
* Robot turns and moves (3 times) resembling a triangle
* Says the area of triangle after drawing triangle
```
draw_a_square(goal: Tuple)
```
* Function that draws a square with X length
* Robot turns and moves (4 times) resembling a square
* Says the area of square after drawing square
```
follow()
```
* Activity
* Function that keeps following the 2nd rigid body around the grid
* Essentially a never ending moveTo function
* Forever loop that adjusts itself to move towards the 2nd rigid body/toy.
```
activity1()
```
* Activity
* Function that asks the student where a certain coordinate is and asks the student to place the "toy" at that coordinate. The robot moves towards the "toy" and responds whether the student is correct. If the student is incorrect, the robot moves to the correct coordinate.
